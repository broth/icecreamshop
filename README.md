# Ice Cream Shop #
Project for assignments of the course "Parallele Systeme" at Hochschule Karlsruhe.

## Tasks ##
Each exercise has its own git tag (see below).

### Task 1 ###
#### Source code ####
Checkout the tagged version with `git checkout task-1`.
#### Documentation ####
The documentation (log-output etc.) can be found at [doc/task-1.md](https://bitbucket
.org/broth/icecreamshop/src/master/doc/task-1.md).

### Task 2 ###
#### Source code ####
Checkout the tagged version with `git checkout task-2`.
#### Documentation ####
The documentation (log-output etc.) can be found at [doc/task-2.md](https://bitbucket
.org/broth/icecreamshop/src/master/doc/task-2.md).

### Task 3 ###
#### Source code ####
Checkout the tagged version with `git checkout task-3`.
#### Documentation ####
The documentation (log-output etc.) can be found at [doc/task-3.md](https://bitbucket
.org/broth/icecreamshop/src/master/doc/task-3.md).

### Task 4 ###
#### Source code ####
Checkout the tagged version with `git checkout task-4`.
#### Documentation ####
The documentation (log-output etc.) can be found at [doc/task-4.md](https://bitbucket
.org/broth/icecreamshop/src/master/doc/task-4.md).
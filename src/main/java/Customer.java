/**
 * Represents a customer which is waiting for ice cream.
 */
public class Customer {
    private String name;
    private long startTime;
    private int priority;

    /**
     * Create a customer which waits for an ice cream and prints his name and the duration to stdout.
     * When priority is not 0, the name is automatically prefixed with "VIP_".
     *
     * @param name     Name of the customer.
     * @param priority Priority of the Customer. 0 means normal customer, 1 means VIP customer.
     */
    public Customer(String name, int priority) {
        this.name = priority == 0 ? name : "VIP_" + name;
        this.priority = priority;
    }

    public String getName() {
        return name;
    }

    public int getPriority() {
        return priority;
    }

    /**
     * Start time measurement for how long the customer has to wait for an ice cream.
     */
    public void startWaiting() {
        this.startTime = System.currentTimeMillis();
        System.out.println(name + " arrived and waits for ice.");
    }

    /**
     * Stop time measurement for how long the customer has to wait for an ice cream.
     */
    public void finish() {
        System.out.println(name + " got ice in " + (System.currentTimeMillis() - startTime) + "ms");
    }
}

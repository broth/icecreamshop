import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * The Ice class contains the logic for concurrent production and consumtion of icecream.
 */
public class Ice {
    private final ReentrantLock icecreamLock = new ReentrantLock();
    private final Condition icecreamCondition = icecreamLock.newCondition();
    private final ReentrantLock customerLock = new ReentrantLock();
    private final Condition customerCondition = customerLock.newCondition();
    // represents produced icecreams
    private int icecreams;
    // represents a request for icecream
    private int customers;

    /**
     * Request an icecream and wait for it.
     *
     * @param customerName Name of the waiting customer to produce icecream for.
     */
    void take(String customerName) {
        // Request production of new icecream
        customerLock.lock();
        try {
            customers++;
            customerCondition.signal();
        } finally {
            customerLock.unlock();
        }

        icecreamLock.lock();
        try {
            while (icecreams == 0) {
                try {
                    System.out.println(customerName + " is waiting for icecream");
                    icecreamCondition.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            icecreams--;
        } finally {
            icecreamLock.unlock();
        }
    }

    /**
     * Produce icecream for a waiting customer.
     *
     * @param employeeName Name of the icecream-producing employee.
     */
    void produce(String employeeName) {
        customerLock.lock();
        try {
            while (customers == 0) {
                try {
                    System.out.println(employeeName + " is waiting for customers");
                    customerCondition.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            customers--;
        } finally {
            customerLock.unlock();
        }

        try {
            // create random int between 3000 - 6000
            int randMillis = ThreadLocalRandom.current().nextInt(3000, 6001);
            System.out.println(employeeName + " starts working");
            Thread.sleep(randMillis);
            System.out.println(employeeName + " produced icecream in " + randMillis + " ms");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        icecreamLock.lock();
        try {
            icecreams++;
            icecreamCondition.signal();
        } finally {
            icecreamLock.unlock();
        }
    }
}

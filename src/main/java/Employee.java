import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Represents an employee of the ice cream shop.
 */
public class Employee implements Runnable {
    private String name;
    private PriorityBlockingQueue<Customer> customers;

    /**
     * Creates a new employee.
     *
     * @param name      The employee's name.
     * @param customers The PriorityBlockingQueue with customers.
     */
    public Employee(String name, PriorityBlockingQueue<Customer> customers) {
        this.customers = customers;
        this.name = name;
    }

    /**
     * Let's the employee wait for a customer an produce an ice cream.
     */
    @Override
    public void run() {
        boolean done = false;
        while (!(Thread.currentThread().isInterrupted())) {
            Customer customer = null;
            try {
                System.out.println(name + " is waiting for customers");
                customer = customers.take();

            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
            if (!Thread.currentThread().isInterrupted()) {
                produceIceCream(customer);
            }
        }

        // Finish all remaining tasks, but dont fetch new ones.
        shutdown();
    }

    //
    private void shutdown() {
        for (Customer customer = customers.poll(); customer != null; customer = customers.poll()) {
            produceIceCream(customer);
        }
    }

    private void produceIceCream(Customer customer) {
        int randMillis = ThreadLocalRandom.current().nextInt(3000, 6001);
        System.out.println(name + " starts working for " + customer.getName());
        try {
            Thread.sleep(randMillis / 10);
        } catch (InterruptedException e) {
            System.out.println(name + " interrupted during production. Finishing up.");
            Thread.currentThread().interrupt();
        }
        System.out.println(name + " produced icecream for " + customer.getName() + " in " + randMillis + " ms");
        customer.finish();
    }
}

import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.ThreadLocalRandom;

import static java.lang.Thread.sleep;

/**
 * Spawns customers in predefined intervals.
 */
public class CustomerSpawner implements Runnable {
    private static final int GROUPS = 10;
    private static int customerCount = 1;
    private PriorityBlockingQueue<Customer> customers;

    public CustomerSpawner(PriorityBlockingQueue<Customer> customers) {
        this.customers = customers;
    }

    @Override
    public void run() {
        // Vormittags
        for (int i = 0; i < GROUPS; i++) {
            spawn(7, 1, 3);
        }
        // Nachmittags
        for (int i = 0; i < GROUPS; i++) {
            spawn(3, 3, 5);
        }
        // Abends
        for (int i = 0; i < GROUPS; i++) {
            spawn(10, 3, 6);
        }
        System.out.println("~~~ Shutting down CustomerSpawner ~~~");
    }

    private void spawn(int minuteInterval, int minCustomers, int maxCustomers) {
        int numCustomers = ThreadLocalRandom.current().nextInt(minCustomers, maxCustomers + 1);
        for (int i = 0; i < numCustomers; i++) {
            String name = "customer_" + customerCount++;
            int vipProbability = ThreadLocalRandom.current().nextInt(0, 100);
            int priority = vipProbability < 40 ? 1 : 0;
            Customer customer = new Customer(name, priority);
            customer.startWaiting();
            customers.add(customer);
        }
        try {
            sleep(minuteInterval * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

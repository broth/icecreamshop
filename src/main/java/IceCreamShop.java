import java.util.Comparator;
import java.util.concurrent.PriorityBlockingQueue;

/**
 * Main class for starting the program.
 */
public class IceCreamShop {

    public static void main(String[] args) {
        PriorityBlockingQueue<Customer> customers = new PriorityBlockingQueue<>(15, Comparator.comparing
                (Customer::getPriority));

        System.out.println("Running with three threads: ");

        Thread francesco = new Thread(new Employee("Francesco", customers));
        Thread paolo = new Thread(new Employee("Paolo", customers));
        Thread michele = new Thread(new Employee("Michele", customers));
        francesco.start();
        paolo.start();
        michele.start();

        new CustomerSpawner(customers).run();

        // Stop threads by sending interrupt. Nit the best solution, beacuse one thread has to finish
        francesco.interrupt();
        paolo.interrupt();
        michele.interrupt();

    }
}

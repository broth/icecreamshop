# Task 4 #
Für eine Priorisierung der Kunden muss anstatt einer *BlockingQueue* eine
*PriorityBlockingQueue* verwendet werden.

## Output of the programm ##
### FixedThreadPool(3) for employees ###
```
Running with three threads: 
Francesco is waiting for customers
Paolo is waiting for customers
Michele is waiting for customers
customer_1 arrived and waits for ice.
customer_2 arrived and waits for ice.
Francesco starts working for customer_1
Michele starts working for customer_2
customer_3 arrived and waits for ice.
Paolo starts working for customer_3
Paolo produced icecream for customer_3 in 3498 ms
customer_3 got ice in 350ms
Paolo is waiting for customers
Francesco produced icecream for customer_1 in 3880 ms
customer_1 got ice in 388ms
Francesco is waiting for customers
Michele produced icecream for customer_2 in 5652 ms
customer_2 got ice in 566ms
Michele is waiting for customers
VIP_customer_4 arrived and waits for ice.
customer_5 arrived and waits for ice.
Paolo starts working for VIP_customer_4
Francesco starts working for customer_5
Francesco produced icecream for customer_5 in 3024 ms
customer_5 got ice in 303ms
Francesco is waiting for customers
Paolo produced icecream for VIP_customer_4 in 3038 ms
VIP_customer_4 got ice in 303ms
Paolo is waiting for customers
VIP_customer_6 arrived and waits for ice.
customer_7 arrived and waits for ice.
Michele starts working for VIP_customer_6
Francesco starts working for customer_7
customer_8 arrived and waits for ice.
Paolo starts working for customer_8
Francesco produced icecream for customer_7 in 3133 ms
customer_7 got ice in 314ms
Francesco is waiting for customers
Michele produced icecream for VIP_customer_6 in 3260 ms
VIP_customer_6 got ice in 327ms
Michele is waiting for customers
Paolo produced icecream for customer_8 in 3633 ms
customer_8 got ice in 364ms
Paolo is waiting for customers
VIP_customer_9 arrived and waits for ice.
Francesco starts working for VIP_customer_9
Francesco produced icecream for VIP_customer_9 in 3151 ms
VIP_customer_9 got ice in 316ms
Francesco is waiting for customers
VIP_customer_10 arrived and waits for ice.
customer_11 arrived and waits for ice.
Michele starts working for VIP_customer_10
Paolo starts working for customer_11
Paolo produced icecream for customer_11 in 3965 ms
customer_11 got ice in 397ms
Paolo is waiting for customers
Michele produced icecream for VIP_customer_10 in 4262 ms
VIP_customer_10 got ice in 427ms
Michele is waiting for customers
VIP_customer_12 arrived and waits for ice.
customer_13 arrived and waits for ice.
Francesco starts working for VIP_customer_12
Paolo starts working for customer_13
Paolo produced icecream for customer_13 in 5610 ms
customer_13 got ice in 561ms
Paolo is waiting for customers
Francesco produced icecream for VIP_customer_12 in 5803 ms
VIP_customer_12 got ice in 581ms
Francesco is waiting for customers
customer_14 arrived and waits for ice.
customer_15 arrived and waits for ice.
Michele starts working for customer_14
Paolo starts working for customer_15
customer_16 arrived and waits for ice.
Francesco starts working for customer_16
Francesco produced icecream for customer_16 in 3270 ms
customer_16 got ice in 328ms
Francesco is waiting for customers
Paolo produced icecream for customer_15 in 3509 ms
customer_15 got ice in 351ms
Paolo is waiting for customers
Michele produced icecream for customer_14 in 3790 ms
customer_14 got ice in 380ms
Michele is waiting for customers
VIP_customer_17 arrived and waits for ice.
VIP_customer_18 arrived and waits for ice.
Francesco starts working for VIP_customer_17
customer_19 arrived and waits for ice.
Paolo starts working for VIP_customer_18
Michele starts working for customer_19
Paolo produced icecream for VIP_customer_18 in 4630 ms
VIP_customer_18 got ice in 464ms
Paolo is waiting for customers
Francesco produced icecream for VIP_customer_17 in 5192 ms
VIP_customer_17 got ice in 520ms
Francesco is waiting for customers
Michele produced icecream for customer_19 in 5872 ms
customer_19 got ice in 587ms
Michele is waiting for customers
VIP_customer_20 arrived and waits for ice.
Paolo starts working for VIP_customer_20
Paolo produced icecream for VIP_customer_20 in 4013 ms
VIP_customer_20 got ice in 402ms
Paolo is waiting for customers
customer_21 arrived and waits for ice.
customer_22 arrived and waits for ice.
Francesco starts working for customer_21
Michele starts working for customer_22
Michele produced icecream for customer_22 in 4954 ms
customer_22 got ice in 495ms
Michele is waiting for customers
Francesco produced icecream for customer_21 in 5280 ms
customer_21 got ice in 529ms
Francesco is waiting for customers
customer_23 arrived and waits for ice.
VIP_customer_24 arrived and waits for ice.
Paolo starts working for customer_23
Michele starts working for VIP_customer_24
customer_25 arrived and waits for ice.
VIP_customer_26 arrived and waits for ice.
Francesco starts working for customer_25
Francesco produced icecream for customer_25 in 3829 ms
customer_25 got ice in 383ms
Francesco is waiting for customers
Francesco starts working for VIP_customer_26
Michele produced icecream for VIP_customer_24 in 5321 ms
VIP_customer_24 got ice in 532ms
Michele is waiting for customers
Paolo produced icecream for customer_23 in 5625 ms
customer_23 got ice in 563ms
Paolo is waiting for customers
Francesco produced icecream for VIP_customer_26 in 5642 ms
VIP_customer_26 got ice in 947ms
Francesco is waiting for customers
customer_27 arrived and waits for ice.
VIP_customer_28 arrived and waits for ice.
customer_29 arrived and waits for ice.
Michele starts working for customer_27
Paolo starts working for VIP_customer_28
Francesco starts working for customer_29
Francesco produced icecream for customer_29 in 3180 ms
customer_29 got ice in 319ms
Francesco is waiting for customers
Paolo produced icecream for VIP_customer_28 in 4129 ms
VIP_customer_28 got ice in 413ms
Paolo is waiting for customers
Michele produced icecream for customer_27 in 4985 ms
customer_27 got ice in 499ms
Michele is waiting for customers
customer_30 arrived and waits for ice.
VIP_customer_31 arrived and waits for ice.
Francesco starts working for customer_30
customer_32 arrived and waits for ice.
Paolo starts working for VIP_customer_31
Michele starts working for customer_32
Francesco produced icecream for customer_30 in 3359 ms
customer_30 got ice in 336ms
Francesco is waiting for customers
Paolo produced icecream for VIP_customer_31 in 3544 ms
VIP_customer_31 got ice in 355ms
Paolo is waiting for customers
Michele produced icecream for customer_32 in 5049 ms
customer_32 got ice in 505ms
Michele is waiting for customers
VIP_customer_33 arrived and waits for ice.
VIP_customer_34 arrived and waits for ice.
Francesco starts working for VIP_customer_33
customer_35 arrived and waits for ice.
Paolo starts working for VIP_customer_34
customer_36 arrived and waits for ice.
Michele starts working for customer_35
customer_37 arrived and waits for ice.
Michele produced icecream for customer_35 in 3855 ms
customer_35 got ice in 386ms
Michele is waiting for customers
Michele starts working for customer_36
Francesco produced icecream for VIP_customer_33 in 4745 ms
VIP_customer_33 got ice in 475ms
Francesco is waiting for customers
Francesco starts working for customer_37
Paolo produced icecream for VIP_customer_34 in 5741 ms
VIP_customer_34 got ice in 574ms
Paolo is waiting for customers
Michele produced icecream for customer_36 in 5079 ms
customer_36 got ice in 893ms
Michele is waiting for customers
Francesco produced icecream for customer_37 in 5875 ms
customer_37 got ice in 1062ms
Francesco is waiting for customers
VIP_customer_38 arrived and waits for ice.
customer_39 arrived and waits for ice.
Paolo starts working for VIP_customer_38
Michele starts working for customer_39
customer_40 arrived and waits for ice.
customer_41 arrived and waits for ice.
customer_42 arrived and waits for ice.
Francesco starts working for customer_40
Paolo produced icecream for VIP_customer_38 in 5057 ms
VIP_customer_38 got ice in 506ms
Paolo is waiting for customers
Paolo starts working for customer_41
Michele produced icecream for customer_39 in 5121 ms
customer_39 got ice in 513ms
Michele is waiting for customers
Michele starts working for customer_42
Francesco produced icecream for customer_40 in 5878 ms
customer_40 got ice in 588ms
Francesco is waiting for customers
Paolo produced icecream for customer_41 in 3430 ms
customer_41 got ice in 848ms
Paolo is waiting for customers
Michele produced icecream for customer_42 in 3971 ms
customer_42 got ice in 909ms
Michele is waiting for customers
customer_43 arrived and waits for ice.
VIP_customer_44 arrived and waits for ice.
Francesco starts working for customer_43
customer_45 arrived and waits for ice.
Paolo starts working for VIP_customer_44
Michele starts working for customer_45
VIP_customer_46 arrived and waits for ice.
customer_47 arrived and waits for ice.
Francesco produced icecream for customer_43 in 3651 ms
customer_43 got ice in 366ms
Francesco is waiting for customers
Francesco starts working for customer_47
Michele produced icecream for customer_45 in 5107 ms
customer_45 got ice in 511ms
Michele is waiting for customers
Michele starts working for VIP_customer_46
Paolo produced icecream for VIP_customer_44 in 5945 ms
VIP_customer_44 got ice in 595ms
Paolo is waiting for customers
Francesco produced icecream for customer_47 in 3886 ms
customer_47 got ice in 753ms
Francesco is waiting for customers
Michele produced icecream for VIP_customer_46 in 5987 ms
VIP_customer_46 got ice in 1109ms
Michele is waiting for customers
customer_48 arrived and waits for ice.
customer_49 arrived and waits for ice.
VIP_customer_50 arrived and waits for ice.
Paolo starts working for customer_48
customer_51 arrived and waits for ice.
VIP_customer_52 arrived and waits for ice.
Francesco starts working for customer_49
Michele starts working for customer_51
Paolo produced icecream for customer_48 in 3134 ms
customer_48 got ice in 314ms
Paolo is waiting for customers
Paolo starts working for VIP_customer_50
Francesco produced icecream for customer_49 in 3349 ms
customer_49 got ice in 334ms
Francesco is waiting for customers
Francesco starts working for VIP_customer_52
Michele produced icecream for customer_51 in 4640 ms
customer_51 got ice in 465ms
Michele is waiting for customers
Paolo produced icecream for VIP_customer_50 in 4895 ms
VIP_customer_50 got ice in 803ms
Paolo is waiting for customers
Francesco produced icecream for VIP_customer_52 in 5283 ms
VIP_customer_52 got ice in 863ms
Francesco is waiting for customers
customer_53 arrived and waits for ice.
customer_54 arrived and waits for ice.
customer_55 arrived and waits for ice.
Michele starts working for customer_53
customer_56 arrived and waits for ice.
Paolo starts working for customer_54
Francesco starts working for customer_55
Francesco produced icecream for customer_55 in 3840 ms
customer_55 got ice in 384ms
Francesco is waiting for customers
Francesco starts working for customer_56
Michele produced icecream for customer_53 in 3962 ms
customer_53 got ice in 397ms
Michele is waiting for customers
Paolo produced icecream for customer_54 in 4603 ms
customer_54 got ice in 461ms
Paolo is waiting for customers
Francesco produced icecream for customer_56 in 3521 ms
customer_56 got ice in 736ms
Francesco is waiting for customers
customer_57 arrived and waits for ice.
customer_58 arrived and waits for ice.
Michele starts working for customer_57
Paolo starts working for customer_58
customer_59 arrived and waits for ice.
customer_60 arrived and waits for ice.
Francesco starts working for customer_59
Michele produced icecream for customer_57 in 3109 ms
customer_57 got ice in 311ms
Michele is waiting for customers
Michele starts working for customer_60
Francesco produced icecream for customer_59 in 4171 ms
customer_59 got ice in 418ms
Francesco is waiting for customers
Paolo produced icecream for customer_58 in 5211 ms
customer_58 got ice in 522ms
Paolo is waiting for customers
Michele produced icecream for customer_60 in 5976 ms
customer_60 got ice in 907ms
Michele is waiting for customers
VIP_customer_61 arrived and waits for ice.
VIP_customer_62 arrived and waits for ice.
customer_63 arrived and waits for ice.
Francesco starts working for VIP_customer_61
customer_64 arrived and waits for ice.
Michele starts working for VIP_customer_62
Paolo starts working for customer_63
customer_65 arrived and waits for ice.
Michele produced icecream for VIP_customer_62 in 3153 ms
VIP_customer_62 got ice in 316ms
Michele is waiting for customers
Michele starts working for customer_64
Francesco produced icecream for VIP_customer_61 in 3722 ms
VIP_customer_61 got ice in 373ms
Francesco is waiting for customers
Francesco starts working for customer_65
Paolo produced icecream for customer_63 in 3842 ms
customer_63 got ice in 385ms
Paolo is waiting for customers
Francesco produced icecream for customer_65 in 3045 ms
customer_65 got ice in 677ms
Francesco is waiting for customers
Michele produced icecream for customer_64 in 5591 ms
customer_64 got ice in 875ms
Michele is waiting for customers
customer_66 arrived and waits for ice.
VIP_customer_67 arrived and waits for ice.
customer_68 arrived and waits for ice.
Paolo starts working for customer_66
Francesco starts working for customer_68
Michele starts working for VIP_customer_67
Michele produced icecream for VIP_customer_67 in 3437 ms
VIP_customer_67 got ice in 344ms
Michele is waiting for customers
Paolo produced icecream for customer_66 in 4565 ms
customer_66 got ice in 457ms
Paolo is waiting for customers
Francesco produced icecream for customer_68 in 5234 ms
customer_68 got ice in 524ms
Francesco is waiting for customers
VIP_customer_69 arrived and waits for ice.
customer_70 arrived and waits for ice.
Michele starts working for VIP_customer_69
customer_71 arrived and waits for ice.
Paolo starts working for customer_70
VIP_customer_72 arrived and waits for ice.
customer_73 arrived and waits for ice.
Francesco starts working for customer_71
customer_74 arrived and waits for ice.
Paolo produced icecream for customer_70 in 3209 ms
customer_70 got ice in 320ms
Paolo is waiting for customers
Paolo starts working for customer_73
Michele produced icecream for VIP_customer_69 in 3390 ms
VIP_customer_69 got ice in 339ms
Michele is waiting for customers
Michele starts working for customer_74
Francesco produced icecream for customer_71 in 5730 ms
customer_71 got ice in 573ms
Francesco is waiting for customers
Francesco starts working for VIP_customer_72
Paolo produced icecream for customer_73 in 4730 ms
customer_73 got ice in 794ms
Paolo is waiting for customers
Michele produced icecream for customer_74 in 5251 ms
customer_74 got ice in 864ms
Michele is waiting for customers
Francesco produced icecream for VIP_customer_72 in 4389 ms
VIP_customer_72 got ice in 1012ms
Francesco is waiting for customers
VIP_customer_75 arrived and waits for ice.
VIP_customer_76 arrived and waits for ice.
Paolo starts working for VIP_customer_75
Michele starts working for VIP_customer_76
VIP_customer_77 arrived and waits for ice.
Francesco starts working for VIP_customer_77
Paolo produced icecream for VIP_customer_75 in 3399 ms
VIP_customer_75 got ice in 340ms
Paolo is waiting for customers
Michele produced icecream for VIP_customer_76 in 5851 ms
VIP_customer_76 got ice in 585ms
Michele is waiting for customers
Francesco produced icecream for VIP_customer_77 in 5981 ms
VIP_customer_77 got ice in 598ms
Francesco is waiting for customers
customer_78 arrived and waits for ice.
VIP_customer_79 arrived and waits for ice.
Paolo starts working for customer_78
VIP_customer_80 arrived and waits for ice.
customer_81 arrived and waits for ice.
Michele starts working for VIP_customer_79
customer_82 arrived and waits for ice.
Francesco starts working for customer_81
customer_83 arrived and waits for ice.
Michele produced icecream for VIP_customer_79 in 4097 ms
VIP_customer_79 got ice in 410ms
Michele is waiting for customers
Michele starts working for customer_82
Paolo produced icecream for customer_78 in 4344 ms
customer_78 got ice in 435ms
Paolo is waiting for customers
Paolo starts working for customer_83
Francesco produced icecream for customer_81 in 4509 ms
customer_81 got ice in 450ms
Francesco is waiting for customers
Francesco starts working for VIP_customer_80
Francesco produced icecream for VIP_customer_80 in 3923 ms
VIP_customer_80 got ice in 842ms
Francesco is waiting for customers
Paolo produced icecream for customer_83 in 5494 ms
customer_83 got ice in 983ms
Paolo is waiting for customers
Michele produced icecream for customer_82 in 5901 ms
customer_82 got ice in 999ms
Michele is waiting for customers
VIP_customer_84 arrived and waits for ice.
customer_85 arrived and waits for ice.
VIP_customer_86 arrived and waits for ice.
Francesco starts working for customer_85
Paolo starts working for VIP_customer_84
customer_87 arrived and waits for ice.
Michele starts working for VIP_customer_86
customer_88 arrived and waits for ice.
customer_89 arrived and waits for ice.
Paolo produced icecream for VIP_customer_84 in 3296 ms
VIP_customer_84 got ice in 330ms
Paolo is waiting for customers
Paolo starts working for customer_87
Michele produced icecream for VIP_customer_86 in 4837 ms
VIP_customer_86 got ice in 493ms
Michele is waiting for customers
Michele starts working for customer_89
Francesco produced icecream for customer_85 in 5417 ms
customer_85 got ice in 542ms
Francesco is waiting for customers
Francesco starts working for customer_88
Michele produced icecream for customer_89 in 4304 ms
customer_89 got ice in 922ms
Michele is waiting for customers
Paolo produced icecream for customer_87 in 5948 ms
customer_87 got ice in 923ms
Paolo is waiting for customers
Francesco produced icecream for customer_88 in 5632 ms
customer_88 got ice in 1104ms
Francesco is waiting for customers
customer_90 arrived and waits for ice.
customer_91 arrived and waits for ice.
Michele starts working for customer_90
VIP_customer_92 arrived and waits for ice.
Paolo starts working for customer_91
VIP_customer_93 arrived and waits for ice.
VIP_customer_94 arrived and waits for ice.
Francesco starts working for VIP_customer_92
customer_95 arrived and waits for ice.
Francesco produced icecream for VIP_customer_92 in 4259 ms
VIP_customer_92 got ice in 426ms
Francesco is waiting for customers
Francesco starts working for customer_95
Michele produced icecream for customer_90 in 4469 ms
customer_90 got ice in 447ms
Michele is waiting for customers
Michele starts working for VIP_customer_93
Paolo produced icecream for customer_91 in 5310 ms
customer_91 got ice in 532ms
Paolo is waiting for customers
Paolo starts working for VIP_customer_94
Francesco produced icecream for customer_95 in 4585 ms
customer_95 got ice in 883ms
Francesco is waiting for customers
Paolo produced icecream for VIP_customer_94 in 3927 ms
VIP_customer_94 got ice in 923ms
Paolo is waiting for customers
Michele produced icecream for VIP_customer_93 in 5392 ms
VIP_customer_93 got ice in 986ms
Michele is waiting for customers
customer_96 arrived and waits for ice.
customer_97 arrived and waits for ice.
VIP_customer_98 arrived and waits for ice.
Francesco starts working for customer_96
customer_99 arrived and waits for ice.
Paolo starts working for customer_97
Michele starts working for customer_99
Michele produced icecream for customer_99 in 5251 ms
customer_99 got ice in 526ms
Michele is waiting for customers
Michele starts working for VIP_customer_98
Francesco produced icecream for customer_96 in 5413 ms
customer_96 got ice in 542ms
Francesco is waiting for customers
Paolo produced icecream for customer_97 in 5543 ms
customer_97 got ice in 555ms
Paolo is waiting for customers
Michele produced icecream for VIP_customer_98 in 3966 ms
VIP_customer_98 got ice in 922ms
Michele is waiting for customers
customer_100 arrived and waits for ice.
customer_101 arrived and waits for ice.
Francesco starts working for customer_100
VIP_customer_102 arrived and waits for ice.
Paolo starts working for customer_101
VIP_customer_103 arrived and waits for ice.
Michele starts working for VIP_customer_102
customer_104 arrived and waits for ice.
Paolo produced icecream for customer_101 in 3927 ms
customer_101 got ice in 392ms
Paolo is waiting for customers
Paolo starts working for customer_104
Francesco produced icecream for customer_100 in 4214 ms
customer_100 got ice in 421ms
Francesco is waiting for customers
Francesco starts working for VIP_customer_103
Michele produced icecream for VIP_customer_102 in 5882 ms
VIP_customer_102 got ice in 588ms
Michele is waiting for customers
Paolo produced icecream for customer_104 in 3012 ms
customer_104 got ice in 694ms
Paolo is waiting for customers
Francesco produced icecream for VIP_customer_103 in 5037 ms
VIP_customer_103 got ice in 925ms
Francesco is waiting for customers
VIP_customer_105 arrived and waits for ice.
VIP_customer_106 arrived and waits for ice.
customer_107 arrived and waits for ice.
Michele starts working for VIP_customer_105
VIP_customer_108 arrived and waits for ice.
VIP_customer_109 arrived and waits for ice.
Paolo starts working for customer_107
Francesco starts working for VIP_customer_108
Michele produced icecream for VIP_customer_105 in 3539 ms
VIP_customer_105 got ice in 354ms
Michele is waiting for customers
Michele starts working for VIP_customer_109
Paolo produced icecream for customer_107 in 3973 ms
customer_107 got ice in 397ms
Paolo is waiting for customers
Paolo starts working for VIP_customer_106
Francesco produced icecream for VIP_customer_108 in 4849 ms
VIP_customer_108 got ice in 484ms
Francesco is waiting for customers
Paolo produced icecream for VIP_customer_106 in 3327 ms
VIP_customer_106 got ice in 729ms
Paolo is waiting for customers
Michele produced icecream for VIP_customer_109 in 5751 ms
VIP_customer_109 got ice in 928ms
Michele is waiting for customers
VIP_customer_110 arrived and waits for ice.
customer_111 arrived and waits for ice.
customer_112 arrived and waits for ice.
Francesco starts working for customer_111
Paolo starts working for customer_112
VIP_customer_113 arrived and waits for ice.
Michele starts working for VIP_customer_110
VIP_customer_114 arrived and waits for ice.
Michele produced icecream for VIP_customer_110 in 3008 ms
VIP_customer_110 got ice in 301ms
Michele is waiting for customers
Michele starts working for VIP_customer_113
Francesco produced icecream for customer_111 in 3731 ms
customer_111 got ice in 374ms
Francesco is waiting for customers
Francesco starts working for VIP_customer_114
Paolo produced icecream for customer_112 in 4249 ms
customer_112 got ice in 425ms
Paolo is waiting for customers
Michele produced icecream for VIP_customer_113 in 3066 ms
VIP_customer_113 got ice in 607ms
Michele is waiting for customers
Francesco produced icecream for VIP_customer_114 in 3203 ms
VIP_customer_114 got ice in 693ms
Francesco is waiting for customers
~~~ Shutting down CustomerSpawner ~~~

```